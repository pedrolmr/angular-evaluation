import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Person } from '../../models/people';
import { PeopleService } from '../../services/people.service';

import { Card } from '../../models/card';
import { CardsService } from '../../services/cards.service';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.scss']
})
export class PersonDetailsComponent implements OnInit {
  @Input() person: Person 
  @Input() cards: Card[]

  constructor(
    private route: ActivatedRoute,
    private peopleService: PeopleService,
    private cardsService: CardsService) { }

  ngOnInit(): void {
    this.getPerson();
    this.getCards();
  }

  getPerson(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.peopleService.getPerson(id)
      .subscribe(person => this.person = person);
  }

  getCards(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.cardsService.getCards(id)
      .subscribe(card => this.cards = card);
  }


  addCard(card_number: string): void {
    const id = +this.route.snapshot.paramMap.get('id');
    card_number = card_number.trim();
    if (!card_number) { return; }
    this.cardsService.addCard(id, { card_number } as Card)
      .subscribe(cards => {
        this.cards.push(cards);
      });
  }
}