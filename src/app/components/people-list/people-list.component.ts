import { Component, OnInit } from '@angular/core';
import { PeopleService } from '../../services/people.service';

import { Person } from '../../models/people';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss']
})
export class PeopleListComponent implements OnInit {
  people: Person[];

  constructor(
    private peopleService: PeopleService) { }

  ngOnInit() {
    this.peopleService.getPeople().subscribe( people => {
      this.people = people;
    })
  }
}

