import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Person } from '../models/people';

@Injectable({
  providedIn: 'root'
})

export class PeopleService {
  private peopleURL: string = 'http://localhost:3000/person';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getPeople(): Observable<Person[]> {
    return this.http.get<Person[]>(`${this.peopleURL}/list`)
  }

  getPerson(id: number): Observable<Person> {
    const url = `${this.peopleURL}/${id}`;
    return this.http.get<Person>(url);
  }
}
