import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { catchError } from 'rxjs/operators';

import { Card } from '../models/card';

@Injectable({
  providedIn: 'root'
})

export class CardsService {
  private cardsURL: string = 'http://localhost:3000/person';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  getCards(id: number): Observable<Card[]> {
    return this.http.get<Card[]>(`${this.cardsURL}/${id}/cards`);
  }

  addCard(id: number, card: Card): Observable<Card> {
    return this.http.post<Card>(`${this.cardsURL}/${id}/cards`, card, this.httpOptions).pipe(
      catchError(this.handleError<Card>('addCard'))
    );
  }

  //handleError function
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      console.log(`${operation} failed: ${error.message}`);
      
      //returns empty result
      return of(result as T);
    };
  }
}
