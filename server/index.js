const express = require('express');
const app = express();
const db = require('./data-service');
const cors = require("cors");

app.use(express.json());
app.use(cors());

//Get list of people
app.get('/person/list', (req, res) => {
  res.json(db.getPeople());
});

//Get individual person
app.get('/person/:id', (req, res) => {
  const { id } = req.params;
  res.json(db.getPerson(id));
});

app.post('/person/update', (req, res) => {
    if (req.body != null && req.body.id != null && req.body.updates != null) res.json(db.updatePerson(req.body.id, req.body.updates));
    else res.json(null);
});

//Get person's cards
app.get('/person/:id/cards', (req, res) => {
  const { id } = req.params;
  
  if (id) {
    res.json(db.getPersonCards(id));
  }else {
    res.json([]);
  };
});

app.post('/card/get', (req, res) => {
  if (req.body != null && req.body.id != null) res.json(db.getCard(req.body.id));
  else res.json(null);
});

//Add a new card to an existing person
app.post('/person/:id/cards', (req, res) => {
  const { id } = req.params;
  const body = req.body;
  if (id && body.card_number){
    res.json(db.addCard(id, body.card_number));
  }else {
    res.json(null);
  };
});

app.listen(3000, () => console.log('server listening on port 3000'));

